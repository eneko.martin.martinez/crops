import pycountry

# list of all countries from pycountry library
alpha2num = {obj.alpha_3: int(obj.numeric) for obj in pycountry.countries}
num2alpha = {int(obj.numeric): obj.alpha_3 for obj in pycountry.countries}
alpha2name = {obj.alpha_3: obj.name for obj in pycountry.countries}
all_countries = set(alpha2num)

region2num = {"EU27": 1001,
              "UK": 1002,
              "China": 1003,
              "EASOC": 1004,
              "India": 1005,
              "LATAM": 1006,
              "Russia": 1007,
              "USMCA": 1008,
              "LROW": 1009}

num2region = {value: key for (key, value) in region2num.items()}

zone2num = {"Tropical": 1, "Hot arid": 2, "Temperate": 3, "Warm": 4,
            "Cold arid": 5, "Polar": 6, "Snow": 7, "Winter snow": 8}

num2zone = {value: key for (key, value) in zone2num.items()}


# group 1
eu27_wili = {"AUT", "BEL", "BGR", "HRV", "CYP", "CZE", "DNK", "EST", "FIN", "FRA", "DEU", "GRC", "HUN", "IRL", "ITA", "LVA", "LTU", "LUX", "MLT", "NLD", "POL", "PRT", "ROU", "SVK", "SVN", "ESP", "SWE"}
# group 2
uk_wili = {"GBR"}
# group 3
china_wili = {"CHN", "HKG"}
# group 4
east_asia_and_oceania_wili = {"AUS", "BRN", "KHM", "TWN", "IDN", "JPN", "KOR", "MYS", "NZL", "PHL", "SGP", "THA", "VNM"}
# group 5
india_wili = {"IND"}
# group 6
latin_america_wili = {"ARG", "BRA", "CHL", "COL", "CRI", "PER"}
# group 7
russia_wili = {"RUS"}
# group 8
us_mexico_and_canada_wili = {"CAN", "MEX", "USA"}
row_minus_others = {"ZAF", "TUR", "TUN", "SAU", "NOR", "MAR", "KAZ", "ISR", "ISL", "CHE"}
others = eu27_wili | uk_wili | china_wili | east_asia_and_oceania_wili | india_wili | latin_america_wili | russia_wili | us_mexico_and_canada_wili | row_minus_others
# group 9
row_wili = row_minus_others | set([x for x in all_countries if x not in others])

assert len(others | row_wili) == len(all_countries)

wili_regions = {"EU27": eu27_wili,
                "UK": uk_wili,
                "China": china_wili,
                "EASOC": east_asia_and_oceania_wili,
                "India": india_wili,
                "LATAM": latin_america_wili,
                "Russia": russia_wili,
                "USMCA": us_mexico_and_canada_wili,
                "LROW": row_wili}