import pandas as pd
from openpyxl import load_workbook
from data_classes import Mask, CropYields
import dask

zones = ["Tropical", "Hot arid", "Temperate",
         "Warm", "Cold arid", "Polar", "Snow", "Winter snow"]
crops = ["Wheat", "Sunflower", "Sugar cane", "Sugabeet",
         "Soy", "Rice", "Rapeseed", "Millet", "Maize",
         "Groundnuts", "Field peas", "Cassava"]
path = "my_fit.xlsx"

Mask.set_aggregation(
    mask_file="mask_clean.nc",
    zones=zones,
    countryregions=None)

writer = pd.ExcelWriter(path, engine='xlsxwriter')


def adjust(crop, irrigated):
    try:
        if crop == "Maize":
            ppm0 = 75
        else:
            ppm0 = 125
        CropYields(
            temperature_folder="Temperature",
            temperature_outfile="tas_average.nc",
            co2_path="CO2 concentration.xlsx",
            RCP=60,
            model="LPJmL",
            crop=crop,
            fixed_co2=False,
            irrigated=irrigated,
            LPF=5,
            min=0,
            max=None
        ).save(writer, ppm0)
    except Exception:
        print("Not able to compute ", crop, " irrigated ", irrigated)


ll = []
for crop in crops:
    for irrigated in [True, False]:
        ll.append(dask.delayed(adjust)(crop, irrigated))

dask.compute(ll)
writer.save()
