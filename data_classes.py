from pathlib import Path
from typing import Union
import numpy as np
import pandas as pd
import xarray as xr
from dataclasses import dataclass
import matplotlib.pyplot as plt
from scipy import linalg
from lmfit import Model
from countries_dicts import alpha2num, region2num, zone2num


@dataclass
class Loading:
    temperature_folder: Path
    temperature_outfile: Path
    co2_path: Path
    RCP: float
    model: str
    crop: str
    fixed_co2: bool
    irrigated: bool
    LPF: int
    min: float
    max: float


@dataclass
class Aggregation:
    mask_file: Path
    zones: Union[None, str, list]
    countryregions: Union[None, str, list]


class Clima():
    """
    Class to save the read Excel files and thus avoid double reading
    """
    allowed_rcps = (26, 60)
    _temperature = {rcp: None for rcp in allowed_rcps}
    _temperature_av = {rcp: {} for rcp in allowed_rcps}
    _co2 = {"2005co2": None, **{rcp: None for rcp in allowed_rcps}}
    _year, _lon, _lat = None, None, None

    @classmethod
    def get(cls, rcp, tas_path, tas_outname, co2_path, co2_fixed, lpf):
        """
        Read the Excel file or return the previously read one
        """
        if rcp not in cls._temperature:
            raise ValueError(f"RCP {rcp} is not configurated.")

        if cls._temperature[rcp] is None:
            cls.load_average_temperature(tas_path, rcp, tas_outname)
            assert cls._temperature[rcp] is not None

        if lpf not in cls._temperature_av[rcp]:
            cls._temperature_av[rcp][lpf] = Mask.compute_average(
                cls._temperature[rcp], lpf)

        return (cls._temperature_av[rcp][lpf],
                cls.get_co2(co2_path, rcp, co2_fixed, lpf))

    @classmethod
    def get_co2(cls, co2_path, rcp, co2_fixed, lpf):
        """
        Get xarray of CO2 concentrarions
        """
        if cls._co2["2005co2"] is None:
            cls.read_co2(co2_path)

        if co2_fixed:
            rcp = "2005co2"

        return cls._co2[rcp].rolling(
            window=lpf, center=True).mean().dropna()

    @classmethod
    def read_co2(cls, co2_path):
        for rcp in cls.allowed_rcps:
            cls._co2[rcp] = pd.read_excel(
                co2_path, f"RCP{rcp}", index_col=0, usecols="A:B")["CO2"]
        cls._co2["2005co2"] = pd.read_excel(
                co2_path, "2005co2", index_col=0, usecols="A:B")["CO2"]

        for series in cls._co2.values():
            series.index.name = "year"

    @classmethod
    def load_average_temperature(cls, path, rcp, outname):
        """Load yearly averaged temperatures"""

        if outname:
            outname = Path(path).joinpath(f'Rcp{rcp}').joinpath(outname)
            if outname.is_file():
                with xr.open_dataset(outname) as tas:
                    cls._temperature[rcp] = tas.tasAdjust[
                        tas.tasAdjust.year <= 2099]
                    cls._year = cls._temperature[rcp].year
                    cls._lon = cls._temperature[rcp].lon
                    cls._lat = cls._temperature[rcp].lat
                return

        years = [
            "20060101-20101231", "20110101-20201231", "20210101-20301231",
            "20310101-20401231", "20410101-20501231", "20510101-20601231",
            "20610101-20701231", "20710101-20801231", "20810101-20901231"]

        if rcp == 26:
            years.append("20910101-21001231")
        else:
            years.append("20910101-20991231")

        paths = Path(path).joinpath(f'Rcp{rcp}')
        paths = [paths.joinpath(
            f'tas_day_HadGEM2-ES_rcp{rcp}_r1i1p1_EWEMBI_landonly_{year}.nc')
            for year in years]

        with xr.open_mfdataset(paths, concat_dim='time',
                               combine='by_coords', parallel=True) as ds:
            tas = ds.groupby('time.year').mean('time')

        tas.load()

        if outname:
            tas.to_netcdf(outname)

        cls._temperature[rcp] = tas.tasAdjust[tas.tasAdjust.year <= 2099]
        cls._year = cls._temperature[rcp].year
        cls._lon = cls._temperature[rcp].lon
        cls._lat = cls._temperature[rcp].lat

    @classmethod
    def clean(cls):
        """
        Clean the dictionary of loaded files
        """
        cls._temperature = {rcp: None for rcp in cls.allowed_rcps}
        cls._temperature_av = {rcp: {} for rcp in cls.allowed_rcps}
        cls._co2 = {"2005co2": None, **{rcp: None for rcp in cls.allowed_rcps}}

    @classmethod
    def show(cls, rcp, lpf):
        if lpf not in cls._temperature_av[rcp]:
            cls._temperature_av[rcp][lpf] = Mask.compute_average(
                cls._temperature[rcp], lpf)
        cls._temperature_av[rcp][lpf].plot()
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        plt.ylabel("Temperature (K)")
        plt.show()
        cls.get_co2(None, rcp, False, lpf).plot()
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        plt.ylabel(r"CO$_2$ (ppm)")
        plt.show()


class Mask():
    """
    Class to save the read Excel files and thus avoid double reading
    """
    _masks = {}
    _loaded = False
    _zones = None
    _countries = None
    _regions = None
    _weights = None

    @classmethod
    def set_aggregation(cls, **aggregation):
        cls.clean()
        cls.aggregation = Aggregation(**aggregation)
        cls._get_masks()

    @classmethod
    def _load(cls):
        """
        Read the mask file if not loaded
        """
        with xr.open_dataset(cls.aggregation.mask_file) as mask:
            cls._zones = mask["zones"]
            cls._countries = mask["countries"]
            cls._regions = mask["regions"]
            cls._weights = mask["weights"]

        cls._loaded = True

    @classmethod
    def _compute_mask(cls, zone, countryregion):
        if not cls._loaded:
            cls._load()
        # compute country or region mask
        if countryregion is not None:
            if countryregion in alpha2num:
                regionmask =\
                    (cls._countries == alpha2num[countryregion]) * cls._weights
            elif countryregion in region2num:
                regionmask =\
                    (cls._regions == region2num[countryregion]) * cls._weights
            else:
                raise KeyError(countryregion)
        else:
            regionmask = cls._weights

        if zone is not None:
            return (cls._zones == zone2num[zone]) * regionmask

        return regionmask

    @classmethod
    def _get_masks(cls):
        """Get all the masks"""

        if cls.aggregation.zones is None\
           or isinstance(cls.aggregation.zones, str):
            zones = [cls.aggregation.zones]
        else:
            zones = cls.aggregation.zones

        if cls.aggregation.countryregions is None\
           or isinstance(cls.aggregation.countryregions, str):
            areas = [(zone, cls.aggregation.countryregions)
                     for zone in zones]
        else:
            areas = [(zone, countryregion)
                     for zone in zones
                     for countryregion in cls.aggregation.countryregions]

        for area in areas:
            if area not in cls._masks:
                cls._masks[area] = cls._compute_mask(area[0], area[1])

    @classmethod
    def compute_average(cls, data, lpf, min=None, max=None):
        """Compute the average values using the mamsk"""
        lpf_data = cls.lpf(data, lpf)

        if min is None:
            min = -np.inf
        if max is None:
            max = np.inf

        filter = np.all(np.logical_and(data > min, data < max), axis=0)

        av_data = pd.DataFrame(index=lpf_data.year)
        for area, mask in cls._masks.items():
            mask_f = mask * filter
            av_data[area] =\
                ((lpf_data * mask_f).sum(('lat', 'lon'))
                 / mask_f.sum())

        return av_data

    @classmethod
    def lpf(cls, data, lpf):
        lpf_data = data.rolling(year=lpf, center=True).mean()
        return lpf_data[np.logical_not(
            np.all(np.isnan(lpf_data), axis=(1, 2))
            )]

    @classmethod
    def show(cls):
        for (zone, region), mask in cls._masks.items():
            mask.plot()
            title = []
            title.append(f"Region: {region}") if region else None
            title.append(f"Zone: {zone}") if zone else None
            plt.title(" / ".join(title))
            plt.show()

    @classmethod
    def clean(cls):
        """
        Clean the dictionary of loaded files
        """
        cls._masks = {}
        cls._loaded = False
        cls._zones = None
        cls._countries = None
        cls._regions = None
        cls._weights = None


class CropYields(object):
    """
    Class to save the crop yield
    """
    def __init__(self, **loading):
        self.loading = Loading(**loading)
        self.temperature, self.co2 = Clima.get(
            self.loading.RCP,
            self.loading.temperature_folder,
            self.loading.temperature_outfile,
            self.loading.co2_path,
            self.loading.fixed_co2,
            self.loading.LPF
        )

        self.compute_crop_yields(
            self.loading.RCP,
            self.loading.model,
            self.loading.crop,
            self.loading.fixed_co2,
            self.loading.irrigated,
            self.loading.LPF,
            self.loading.min,
            self.loading.max
        )

    def save(self, writer, b20=100):
        self.adjust(b20).to_excel(writer, sheet_name=self.var_name)

    def adjust(self, b20=100):
        t0 = self.temperature.iloc[0, :]
        dt = self.temperature - t0
        co20 = self.co2.iloc[0]
        dco2 = self.co2 - co20
        y0 = self._data.iloc[0, :]
        y = self._data/y0 - 1

        def damage(dT, dCO2, a1, a2, b1, b2):

            return a1*dT+a2*dT**2+b1*dCO2/(b2+dCO2)

        result = pd.DataFrame(
            index=[col[0] for col in dt.columns],
            data={
                "Y0 (t ha-1 yr-1)": y0.values,
                "T0 (K)": t0.values,
                "CO20 (ppm)": co20,
                "year0": y.index[0]
            }
        )
        for col in dt.columns:
            if np.isnan(y[col].values).any():
                for val in ["a1", "a2", "b1", "b2"]:
                    result.loc[col[0], val] = np.nan
                    result.loc[col[0], "stderr-"+val] = np.nan
                continue
            XX = np.vstack([
                dt[col].values,
                dt[col].values**2,
                dco2.values/(b20+dco2.values)
            ]).T
            p_no_offset = linalg.lstsq(XX, y[col].values)[0]

            model = Model(damage, ['dT', 'dCO2'])
            model.set_param_hint("b1", min=0)
            model.set_param_hint("b2", min=50, max=150)
            model.set_param_hint("a1", min=-0.5, max=0.5)
            model.set_param_hint("a2", min=-0.5, max=0.5)
            res = model.fit(
                y[col].values,
                a1=p_no_offset[0], a2=p_no_offset[1],
                b1=p_no_offset[2], b2=b20,
                dT=dt[col].values, dCO2=dco2.values
            )
            for val in ["a1", "a2", "b1", "b2"]:
                result.loc[col[0], val] = res.params[val].value
                result.loc[col[0], "stderr-"+val] = res.params[val].stderr

        return result

    def compute_crop_yields(self, rcp, model, crop, fixed_co2,
                            irrigated, lpf, min, max):
        data = self.load_crop_yields(rcp, model, crop, fixed_co2, irrigated)
        # asset the grid of the loaded data is equal to the temperature
        # and mask grid
        try:
            assert data.lat.equals(Mask._weights.lat)
            assert data.lon.equals(Mask._weights.lon)
        except AttributeError:
            raise ValueError(
                "You need to define the Mask before any Yield object")
        assert data.lat.equals(Clima._lat)
        assert data.lon.equals(Clima._lon)
        assert data.year.equals(Clima._year)
        self._data = Mask.compute_average(data, lpf, min, max)

    def load_crop_yields(self, rcp, model, crop, fixed_co2, irrigated):
        """Load crop yields"""

        path = Path(model)
        filename = [model.lower(), "hadgem2-es", "ewembi",
                    f"rcp{rcp}", "2005soc"]

        if fixed_co2:
            path = path.joinpath(f"Rcp{rcp} - 2005 CO2")
            filename.append("2005co2")
        else:
            path = path.joinpath(f"Rcp{rcp} - CO2")
            filename.append("co2")

        path = path.joinpath(crop)

        if crop == "Sugabeet":
            crop_name = "sgb"
        elif crop == "Groundnuts":
            crop_name = "nut"
        elif crop == "Field peas":
            crop_name = "pea"
        else:
            crop_name = crop[:3].lower()
        if irrigated:
            self.var_name = "-".join(["yield", crop_name, "firr"])
        else:
            self.var_name = "-".join(["yield", crop_name, "noirr"])

        filename += [self.var_name, "global", "annual", "2006", "2099.nc4"]
        filename = '_'.join(filename)

        with xr.open_dataset(path.joinpath(filename),
                             decode_times=False) as ds:
            ds = ds.rename({"time": "year"})
            ds["year"] = np.arange(2006, 2100)
            data = ds[self.var_name]
        return data

    def show(self):
        self._data.plot()
        plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        plt.ylabel(r"Crop yield (t ha$^{-1}$ yr$^{-1}$)")
        plt.show()

    def __str__(self):
        return "Yield object:\n\t" + str(self.loading)
